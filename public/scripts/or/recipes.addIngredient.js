$(document).ready(function() {

var MaxInputs       = 8; //maximum input boxes allowed
var InputsWrapper   = $("#ingredients-wrapper"); //Input boxes wrapper ID
var AddButton       = $("#AddMoreFileBox"); //Add button ID

var x = InputsWrapper.length; //initlal text box count
var FieldCount=1; //to keep track of text box added

$(AddButton).click(function (e)  //on add input button click
{
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++; //text box added increment
            //add input box
            //$(InputsWrapper).append('<div><input type="text" name="mytext[]" id="field_'+ FieldCount +'" value="Text '+ FieldCount +'"/><a href="#" class="removeclass">&times;</a></div>');
            $(InputsWrapper).append('<div class="form-group"><label for="ingredients['+ FieldCount +'][ing_name]" class="col-sm-2 control-label">Name</label><div class="col-sm-3"><input class="form-control" name="ingredients['+ FieldCount +'][ing_name]" type="text" id="ingredients['+ FieldCount +'][ing_name]"></div><label for="ingredient['+ FieldCount +'][ing_qty]" class="col-sm-1 control-label">Qty.</label><div class="col-sm-1"><input class="form-control" name="ingredients['+ FieldCount +'][ing_qty]" type="text"></div><label for="ingredients['+ FieldCount +']" class="col-sm-1 control-label">Unit</label><div class="col-sm-3"><select class="form-control" name="ingredients['+ FieldCount +'][ing_unit]"><option value="0">Each</option><option value="1">Teaspoon</option><option value="2">Tablespoon</option><option value="3">Grams</option><option value="4">Ounces</option><option value="5">Pounds</option><option value="6">Pint</option><option value="7">Quart</option><option value="8">Gallon</option></select></div></div>');
            x++; //text box increment
        }
return false;
});

$("body").on("click",".removeclass", function(e){ //user click on remove text
        if( x > 1 ) {
                $(this).parent('div').remove(); //remove text box
                x--; //decrement textbox
        }
return false;
}) 

});

