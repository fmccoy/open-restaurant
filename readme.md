Open Restaurant
===============

Open Restaurant is a web based application for managing a restaurant using the Laravel framework.

Features
--------

- Employee Directory
- Employee Schedules
- Inventory Management
- Recipe Management
- Daily Prep

