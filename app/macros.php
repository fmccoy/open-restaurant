<?php

/*
|--------------------------------------------------------------------------
| Macros
|--------------------------------------------------------------------------
|
| Description needed.
|
*/

Form::macro('date', function($name, $value = null, $options = array())
{
	$type = 'type="date"';
	$named = sprintf('name="%s"', $name);
	$id = sprintf('id="%s"', $name);
	//$name = 'name="'.$name.'"';
	//$id = 'id="'.$name.'"';
	$value = isset($value) ? ' value="'.$value.'"' : '';
	$class = array_key_exists('class', $options) ? ' class="'.$options["class"].'"' : '';
	$placeholder = array_key_exists('placeholder', $options) ? ' placeholder="'.$options["placeholder"].'"' : '';
	//$disabled = array_key_exists('disabled', $options) and $options['disabled']== true ? ' disabled' : '';
	
	// Disabled
	if( array_key_exists('disabled', $options) ){
		if($options['disabled'] == true){
			$disabled = 'disabled';
		} else {
			$disabled = "";
		}
	} else {
		$disabled = "";
	}

	$input = sprintf('<input %s %s %s %s %s %s %s>',
		$type,
		$named,
		$id,
		$class,
		$value,
		$placeholder,
		$disabled);



	//return '<input type="date" id="'.$name.'" name="'.$name.'"'.$value.'" "'.$class.'>';
	return $input;
});