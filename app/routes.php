<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

View::composer('layouts.master', 'SiteComposer');


Route::get('/', array( 'as' => 'home', 'uses'=>'HomeController@index'));
//Route::get('dashboard', array('as'=>'dashboard', 'uses'=>'DashboardController@index'));

Route::group(array('before' => 'auth'), function(){

    Route::get('dashboard', array('as'=>'dashboard', 'uses'=>'DashboardController@index'));

    Route::resource('schedules', 'SchedulesController');

    Route::resource('employees', 'EmployeesController');

    Route::resource('inventory', 'InventoryController');

    Route::resource('recipes', 'RecipesController');
});

// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', array('as'=>'logout','uses'=>'UsersController@logout'));

Route::get('login', array('as'=>'login','uses' => 'UsersController@login'));
Route::post('login', 'UsersController@doLogin');
