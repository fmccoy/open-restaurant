<?php

class RecipesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Get All Recipes
		$recipes = Recipe::all();

		// Make View
		$this->layout->content = View::make('recipes.home')->with('recipes', $recipes );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Ingredients
		$ingredients = Input::old('ingredients', array());

		// Make View
		$this->layout->content = View::make('recipes.create')
			->with('ingredients', $ingredients);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Validate
		$rules = array(
			'name'				=>	'required',
			//'ingredients'	=>	'required',
			'category'		=>	'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			$input = Input::all();
			if($input){
				echo "<pre>";
				print_r($input);
				echo "</pre>";
			}

			//return Redirect::to('recipes/create')
				//->withErrors($validator)
				//->withInput(Input::all());
		} else {

			$recipe = new Recipe;
			$recipe->name = Input::get('name');
			$recipe->ingredients = json_encode(Input::get('ingredients'));
			$recipe->category = Input::get('category');
			$recipe->save();

			// Redirect
			Session::flash('message', 'Successfully created Recipe!');
			return Redirect::to('recipes');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get the Recipe
		$recipe = Recipe::find($id);

		// Make View with Recipe
		$this->layout->content = View::make('recipes.show')->with('recipe', $recipe);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// Get the Recipe
		$recipe = Recipe::find($id);

		$ingredients = Input::old('ingredients', json_decode($recipe->ingredients, true));

		// Make View with Recipe
		$this->layout->content = View::make('recipes.edit')
			->with('recipe', $recipe)
			->with('ingredients', $ingredients );
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Validate
		$rules = array(
			'name'				=>	'required',
			//'ingredients'	=>	'required',
			'category'		=>	'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('recipes/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			$recipe = Recipe::find($id);

			$recipe->name = Input::get('name');

			$useringredients = Input::get('ingredients');

			foreach($useringredients as $key => $value){
				
				if($useringredients[$key]['ing_name'] != ''){
					$ping = array(
						'ing_name' => $useringredients[$key]['ing_name'],
						'ing_qty' => $useringredients[$key]['ing_qty'],
						'ing_unit' => $useringredients[$key]['ing_unit']
					);
					$ingredients[] = $ping;
					
				} else {
					unset($useringredients[$key]);
				}
			}

			$recipe->ingredients = json_encode($ingredients);
			$recipe->category = Input::get('category');
			$recipe->save();

			// Redirect
			Session::flash('message', 'Successfully updated Recipe!');
			return Redirect::to('recipes');
			
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// Find and Delete
		$recipe = Recipe::find($id);
		$recipe->delete();

		// Redirect
		Session::flash('message', 'Successfully deleted Recipe');
	}
}
