<?php

class BaseController extends Controller {

	protected $layout = 'layouts.master';
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	public function __construct()
	{
		$siteOptions = array(
			'site_name' => 'Open Restaurant',
			'site_url' => 'http://my.open.res',
		);
		
		Site::setOptions($siteOptions);

		Site::menu($admin = true);
	} 



	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function getMenu()
	{

	}

}