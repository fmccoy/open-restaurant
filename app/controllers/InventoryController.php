<?php

class InventoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Get All Inventory
		$inventory = Inventory::all();

		// Make View
		$this->layout->content = View::make('inventory.home')->with('inventory', $inventory );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Make View
		$this->layout->content = View::make('inventory.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Validate
		$rules = array(
			'name'									=> 'required',
			'purchasing_unit_qty'		=> 'required',
			'purchasing_unit_price'	=> 'required',
			'ind_count_unit'				=> 'required',
			'on_hand'								=> 'required',
			'inventory_conversion'	=> 'numeric|min:1',
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('inventory/create')
				->withErrors($validator)
				->withInput(Input::all());
		} else {

			$inventory = new Inventory;
			
			$inventory->name = Input::get('name');
			$inventory->purchasing_unit_qty = Input::get('purchasing_unit_qty');
			$inventory->purchasing_unit = Input::get('purchasing_unit');
			$inventory->purchasing_unit_price = Input::get('purchasing_unit_price');
			$inventory->recipe_unit = Input::get('recipe_unit');
			$inventory->recipe_conversion = Input::get('recipe_conversion');
			$inventory->ind_count_unit = Input::get('ind_count_unit');
			$inventory->inventory_conversion = Input::get('inventory_conversion');
			$inventory->on_hand = Input::get('on_hand');

			$inventory->save();

			// Redirect
			Session::flash('message', 'Successfully created Inventory!');
			return Redirect::to('inventory');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get the Inventory
		$inventory = Inventory::find($id);

		// Make View with Inventory
		$this->layout->content = View::make('inventory.show')->with('inventory', $inventory);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// Get the Inventory
		$inventory = Inventory::find($id);

		// Make View with Inventory
		$this->layout->content = View::make('inventory.edit')->with('inventory', $inventory);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Validate
		$rules = array(
			'name'		=>	'required',
			'purchasing_unit_qty'			=>	'required',
			'purchasing_unit_price'		=> 	'required',
			'ind_count_unit'	=>	'required',
			'on_hand'			=>	'required',
			'inventory_conversion'	=> 'numeric|min:1',
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('inventory/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::all());
		} else {
			$inventory = Inventory::find($id);

			$inventory->name = Input::get('name');
			$inventory->purchasing_unit_qty = Input::get('purchasing_unit_qty');
			$inventory->purchasing_unit = Input::get('purchasing_unit');
			$inventory->purchasing_unit_price = Input::get('purchasing_unit_price');
			$inventory->recipe_unit = Input::get('recipe_unit');
			$inventory->recipe_conversion = Input::get('recipe_conversion');
			$inventory->ind_count_unit = Input::get('ind_count_unit');
			$inventory->inventory_conversion = Input::get('inventory_conversion');
			$inventory->on_hand = Input::get('on_hand');

			$inventory->save();

			// Redirect
			Session::flash('message', 'Successfully updated Inventory!');
			return Redirect::to('inventory');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// Find and Delete
		$inventory = Inventory::find($id);
		$inventory->delete();

		// Redirect
		Session::flash('message', 'Successfully deleted Inventory');
	}
}
