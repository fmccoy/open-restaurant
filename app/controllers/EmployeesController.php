<?php

class EmployeesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Get All Employees
		$employees = Employee::all();

		// Make View
		$this->layout->content = View::make('employees.home')->with('employees', $employees );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Make View
		$this->layout->content = View::make('employees.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Validate
		$rules = array(
			'first_name'		=>	'required',
			'last_name'			=>	'required',
			'email'					=> 	'required|email',
			'phone_number'	=>	'required:digits:10',
			'position'			=>	'required',
			'availability'	=>	'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('employees/create')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {
			//	Store
			$newUser = new User;
			$newUser->email = Input::get('email');
			$newUser->username = Input::get('first_name') . Input::get('last_name');
			$newUser->password = Hash::make('password');
			$newUser->remember = false;
			$newUser->save();

			$newEmployee = new Employee;
			$newEmployee->user_id = $newUser->id;
			$newEmployee->first_name = Input::get('first_name');
			$newEmployee->last_name = Input::get('last_name');
			$newEmployee->email = $newUser->email;
			$newEmployee->phone_number = Input::get('phone_number');
			$newEmployee->emergency_contact = Input::get('emergency_contact', " ");
			$newEmployee->availability = json_encode(Input::get('availability'));
			$newEmployee->position = Input::get('position');
			$newEmployee->save();

			// Redirect
			Session::flash('message', 'Successfully created Employee!');
			return Redirect::to('employees');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get the Employee
		$employee = Employee::find($id);

		// Make View with Employee
		$this->layout->content = View::make('employees.show')->with('employee', $employee);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		// Get the Employee
		$employee = Employee::find($id);

		// Make View with Employee
		$this->layout->content = View::make('employees.edit')->with('employee', $employee);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Validate
		$rules = array(
			'first_name'		=>	'required',
			'last_name'			=>	'required',
			'email'					=> 	'required|email',
			'phone_number'	=>	'required:digits:10',
			'position'			=>	'required',
			'availability'	=>	'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('employees/'.$id.'/edit')
				->withErrors($validator)
				->withInput(Input::except('password'));
		} else {
			$employee = Employee::find($id);

			$employee->first_name   = Input::get('first_name');
			$employee->last_name    = Input::get('last_name');
			$employee->email        = Input::get('email');
			$employee->phone_number = Input::get('phone_number');
			$employee->availability = json_encode(Input::get('availability'));
			$employee->position     = Input::get('position');
			$employee->save();

			// Redirect
			Session::flash('message', 'Successfully updated Employee!');
			return Redirect::to('employees');
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		// Find and Delete
		$employee = Employee::find($id);
		$employee->delete();

		// Redirect
		Session::flash('message', 'Successfully deleted Employee');
	}
}
