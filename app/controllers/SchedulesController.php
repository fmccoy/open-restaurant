<?php

class SchedulesController extends \BaseController {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$schedules = Schedule::all();
		$week = Schedule::getWeek();
		$available = Schedule::whosAvailable('foh', array('day' => 'tuesday', 'time'=>'pm'));

		$emp = Employee::whatDepartment(1);
		echo '<pre><h1>What Department</h1>';
		print_r($emp);
		echo '</pre>';

		// Make View
		$this->layout->content = View::make('schedules.home')
			->with('schedules', $schedules )
			->with('available', $available )
			->with('week', $week );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$week = Schedule::getWeek();
		$available = Schedule::whosAvailable('boh');
		// Make View
		$this->layout->content = View::make('schedules.create')
			->with('week', $week)
			->with('available', $available);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
