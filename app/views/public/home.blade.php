@section('content')
<br />
<br />
<br />
<br />
<div class="col-sm-8">

    <article>

        <p>Open Restaurant is a web based application for managing a restaurant using the Laravel framework.</p>

        <h2 id="features"><a name="user-content-features" href="#features" class="headeranchor-link"  aria-hidden="true"><span class="headeranchor"></span></a>Features</h2>
        <ul>
            <li>Employee Directory</li>
            <li>Employee Schedules</li>
            <li>Inventory Management</li>
            <li>Recipe Management</li>
            <li>Daily Prep</li>
        </ul>
    </article>

</div>


@stop

@section('sidebar')
<div class="col-sm-4">
    @include('layouts.partials.login-form')
</div>
@stop
