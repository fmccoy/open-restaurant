@section('content')

	<h1>Schedules</h1>

	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th> </th>
					@foreach($week as $day )
					 <th>{{ $day }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="shift">AM</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
				</tr>
				<tr>
					<td class="shift">PM</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
				</tr>
			</tbody>
			<tbody>
				<tr>
					<td class="shift">AM</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>
						<ul>
							<li>Not Assigned</li>
							<li>Not Assigned</li>
							<li>Not Assigned</li>
						</ul>
					</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
				</tr>
				<tr>
					<td class="shift">PM</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
					<td>Not Assigned</td>
				</tr>
			</tbody>
		</table>
	</div>

	<pre>
		<?php print_r($available); ?>
	</pre>

@stop