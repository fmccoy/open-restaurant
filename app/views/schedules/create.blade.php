@section('content')
	<h1>Create Schedule</h1>

	{{ HTML::ul($errors->all()) }}

	{{ Form::open(array( 'url' => 'schedules', 'class'=>'form form-horizontal')) }}

		<div class="form-group">
			{{ Form::label('week_start', 'Week Start:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::date('week_start', Input::old('week_start'), array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('department', 'Department:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{	Form::select('department', array(
					'foh'=> 'Front of House',
					'boh'=> 'Back of House'
				), null, array('class' => 'form-control'))
				}}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('schedule', 'Schedule:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				<div class="table table-responsive">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th> </th>
								@foreach($week as $day )
								 <th>{{ $day }}</th>
								@endforeach
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="shift">AM</td>
								@foreach($week as $day )
								 <th>
								 	<td>
								 		{{ Form::select("schedule[".$day."][am]", $available[$day]['am'],
								 			Input::old('schedule['.$day.'][am]', true ),array(
								 				'class' => 'form-control'
								 			))
								 		}}
								 	</td>
								 </th>
								@endforeach
							</tr>
							<tr>
								<td class="shift">PM</td>
								@foreach($week as $day )
								 <th><td>{{ Form::select("schedule[".$day."][pm]",array(0 =>'NO',1=>'YES'),Input::old('schedule['.$day.'][pm]', true ),array('class'=>'form-control')) }}</td></th>
								@endforeach
							</tr>
						</tbody>
					</table>
				</div>
			</div>			
		</div>

		{{ Form::submit('Submit', array('class' => 'btn btn-default')) }}

	{{ Form::close() }}


@stop

@section('orfooter')
@stop