@section('content')

	<div class="col-sm-3">
		<h2>Daily Prep</h2>
		<ul>
			<li>Needed 1</li>
			<li>Needed 2</li>
			<li>Needed 3</li>
			<li>Needed 4</li>
		</ul>
	</div>

	<div class="col-sm-3">
		<h2>Scheduled In</h2>
		<ul>
			<li>Scheduled 1</li>
			<li>Scheduled 2</li>
			<li>Scheduled 3</li>
			<li>Scheduled 4</li>
		</ul>
	</div>

	<div class="col-sm-3">
		<h2>Low Stock</h2>
		<ul>
			<li>Low Stock 1</li>
			<li>Low Stock 2</li>
			<li>Low Stock 3</li>
			<li>Low Stock 4</li>
		</ul>
	</div>

@stop