@section('content')
	<h1>Inventory <small><a href="{{URL::to('inventory/create')}}">Add Item</a></small></h1>
	<table class="table">
		<thead>
			<tr>
				<th>Product ID</th>
				<th>Product Name</th>
				<th>Unit Price</th>
				<th>On Hand</th>
				<th>Total</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			@foreach($inventory as $inv)
				<tr>
					<td>{{ $inv->id }}</td>
					<td>{{ $inv->name }}</td>
					<td>{{ Inventory::money($inv->purchasing_unit_price / $inv->inventory_conversion) }}</td>
					<td>{{ $inv->on_hand }} {{ $inv->ind_count_unit }}</td>
					<td>{{ Inventory::money($inv->purchasing_unit_price / $inv->inventory_conversion * $inv->on_hand) }}</td>
					<td>
						{{ Form::open(array('url' => 'inventory/' . $inv->id, 'class' => 'pull-right')) }}
							{{ Form::hidden('_method', 'DELETE') }}
							<button type="submit" class="btn btn-warning">
							  <span class="glyphicon glyphicon-trash"></span>
							</button>
						{{ Form::close() }}

						<a class="btn btn-small btn-success" href="{{ URL::to('inventory/' . $inv->id) }}">
							<span class="glyphicon glyphicon-info-sign"></span>
						</a>

						<a class="btn btn-small btn-info" href="{{ URL::to('inventory/' . $inv->id . '/edit') }}">
							<span class="glyphicon glyphicon-edit"></span>
						</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop