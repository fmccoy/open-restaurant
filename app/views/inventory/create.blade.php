@section('content')
	<h1>Add Item</h1>

	{{ HTML::ul($errors->all()) }}

	{{ Form::open(array('class'=>'form form-horizontal','url' => 'inventory')) }}

		<div class="form-group">
			{{ Form::label('name', 'Item Name:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('name', Input::old('name'), array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('purchasing_unit_qty', 'Unit Qty.:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('purchasing_unit_qty', Input::old('purchasing_unit_qty'), array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('purchasing_unit', 'Purchasing Unit:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{	Form::select('purchasing_unit', array(
					'TSP' => 'Teaspoon',
					'TBSP' =>'Tablespoon',
					'EA' => 'Each',
					'OZ' => 'Ounce(s)',
					'LB' => 'Pounds(s)',
					'FLOZ' => 'Fluid Ounce(s)',
					'CUP' => 'Cup(s)',
					'PINT' =>'Pint(s)',
					'QUART' => 'Quart(s)',
					'GALLON' => 'Gallon(s)'
				), null, array('class' => 'form-control'))
				}}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('purchasing_unit_price', 'Unit Price:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('purchasing_unit_price', Input::old('purchasing_unit_price'), array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('recipe_unit', 'Recipe Unit:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{	Form::select('recipe_unit', array(
					'TSP' => 'Teaspoon',
					'TBSP' =>'Tablespoon',
					'EA' => 'Each',
					'OZ' => 'Ounce(s)',
					'LB' => 'Pounds(s)',
					'FLOZ' => 'Fluid Ounce(s)',
					'CUP' => 'Cup(s)',
					'PINT' =>'Pint(s)',
					'QUART' => 'Quart(s)',
					'GALLON' => 'Gallon(s)'
				), null, array('class' => 'form-control'))
				}}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('recipe_conversion', 'Recipe Conversion:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('recipe_conversion', Input::old('recipe_conversion'), array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('ind_count_unit', 'Individual Unit Count:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{	Form::select('ind_count_unit', array(
					'TSP' => 'Teaspoon',
					'TBSP' =>'Tablespoon',
					'EA' => 'Each',
					'OZ' => 'Ounce(s)',
					'LB' => 'Pounds(s)',
					'FLOZ' => 'Fluid Ounce(s)',
					'CUP' => 'Cup(s)',
					'PINT' =>'Pint(s)',
					'QUART' => 'Quart(s)',
					'GALLON' => 'Gallon(s)'
				), null, array('class' => 'form-control'))
				}}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('inventory_conversion', 'Inventory Conversion:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-9">
				{{ Form::text('inventory_conversion', Input::old('inventory_conversion'), array('class'=>'form-control')) }}
			</div>
			<div class="col-sm-1">
				<button type="button" class="btn btn-default">
					<span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right" title="What is the smallest unit for counting."></span>
				</button>
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('on_hand', 'On Hand:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('on_hand', Input::old('on_hand'), array('class'=>'form-control')) }}
			</div>
		</div>

		{{ Form::submit('Submit', array('class' => 'btn btn-default')) }}

	{{ Form::close() }}
@stop