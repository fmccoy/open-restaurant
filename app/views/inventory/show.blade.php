@section('content')
	<h1>Inventory</h1>

	<p>Product ID: {{ $inventory->id }}</p>
	<p>Product Name: {{ $inventory->name }}</p>
	<p>Unit Price: {{ Inventory::money($inventory->purchasing_unit_price / $inventory->inventory_conversion) }}</p>
	<p>On Hand: {{ $inventory->on_hand }}</p>
	<p>On Hand Value: {{ Inventory::money($inventory->purchasing_unit_price / $inventory->inventory_conversion * $inventory->on_hand) }}</p>

@stop