@section('content')
	<h1>Edit Recipe</h1>

	{{ HTML::ul($errors->all()) }}

	{{ Form::model($recipe,array('route'=>array('recipes.update',$recipe->id),'method'=>'PUT', 'class'=>'form form-horizontal' )) }}
		<div class="form-group">
			{{ Form::label('name', 'Recipe Name:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('name', null, array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('category', 'Category:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{	Form::select('category', array(
					'Sauces',
					'Spices',
					'Dressings',
					'Sides',
					'Hot Prep',
					'Cold Prep',
				), null, array('class' => 'form-control'))
				}}
			</div>
		</div>

		<fieldset id="ingredients-wrapper">


			<h2>Ingredients <small><a href="#" id="AddMoreFileBox">Add Ingredient</a></small></h2>

			<?php print_r($ingredients); ?>

			@if( empty($ingredients) == FALSE )
				@foreach($ingredients as $key => $value )
				
				<div class="form-group">
					
					{{ Form::label('ingredients['.$key.'][ing_name]', 'Name', array('class' => 'col-sm-2 control-label'))}}
					<div class="col-sm-3">
						{{ Form::text('ingredients['.$key.'][ing_name]', Input::old('ingredients['.$key.'][ing_name]', $ingredients[$key]['ing_name']), array('class'=>'form-control'))}}
					</div>
					{{ Form::label('ingredient['.$key.'][ing_qty]', 'Qty.', array('class' => 'col-sm-1 control-label'))}}
					<div class="col-sm-1">
						{{ Form::text('ingredients['.$key.'][ing_qty]', Input::old('ingredients['.$key.'][ing_qty]', $ingredients[$key]['ing_qty']), array('class'=>'form-control'))}}
					</div>
					{{ Form::label('ingredients['.$key.']', 'Unit', array('class' => 'col-sm-1 control-label'))}}
					<div class="col-sm-3">
						{{ Form::select('ingredients['.$key.'][ing_unit]', array(
							'Each',
							'Teaspoon',
							'Tablespoon',
							'Grams',
							'Ounces',
							'Pounds',
							'Pint',
							'Quart',
							'Gallon'
						), $ingredients[$key]['ing_unit'], array('class' => 'form-control')) }}
					</div>

				</div>

				@endforeach
			@endif

		</fieldset>

		{{ Form::submit('Submit', array('class' => 'btn btn-default')) }}

	{{ Form::close() }}
@stop


@section('orfooter')
	{{ HTML::script('scripts/or/recipes.addIngredient.js') }}
@stop