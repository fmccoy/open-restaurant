@section('content')

<h1>Recipes</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Category</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($recipes as $recipe)
    	<tr>
    		<td>{{ $recipe->id }}</td>
    		<td>{{ $recipe->name }} {{ $recipe->last_name }}</td>
    		<td>{{ $recipe->category }}</td>
    		<td>
    			{{ Form::open(array('url' => 'emp/' . $recipe->id, 'class' => 'pull-right')) }}
    				{{ Form::hidden('_method', 'DELETE') }}
    				<button type="submit" class="btn btn-warning">
                        <span class="glyphicon glyphicon-trash"></span>
                    </button>
    			{{ Form::close() }}

    			<a class="btn btn-small btn-success" href="{{ URL::to('recipes/' . $recipe->id) }}">     <span class="glyphicon glyphicon-info-sign"></span>
                </a>

    			<a class="btn btn-small btn-info" href="{{ URL::to('recipes/' . $recipe->id . '/edit') }}">
                    <span class="glyphicon glyphicon-edit"></span>
                </a>
    		</td>
    	</tr>
    @endforeach
	</tbody>
</table>

@stop