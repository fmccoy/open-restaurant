@include('layouts.partials.header')

@yield('content')

@yield('sidebar')

@include('layouts.partials.footer')