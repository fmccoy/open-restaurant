{{ HTML::element( 'ul', array('class'=>'nav navbar-nav navbar-right') ) }}

    @foreach( $menu->getItems() as $item )

        {{ View::make('packages.tlr.bootstrap.item')->with( 'item', $item )->render() }}

    @endforeach

</ul>
