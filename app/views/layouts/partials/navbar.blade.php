<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      {{ link_to_route('dashboard', $site['site_name'], array(),array( 'class'=>'navbar-brand')) }}
    </div>
    <div class="navbar-collapse collapse">

      {{ Menu::menu('main-nav') }}

    </div><!--/.nav-collapse -->
  </div>
</div>