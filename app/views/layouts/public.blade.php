<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

    <title>{{ Site::$options['site_name'] }} - {{ Site::$options['site_url'] }}</title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('styles/css/bootstrap.css') }}
    {{ HTML::style('styles/css/custom.css') }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      {{ HTML::style('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
      {{ HTML::style('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
    <![endif]-->

    @yield('orhead', '<!-- Nothing Defined -->')

  </head>

  <body>

    <div class="container">

        <h1>Open Restaurant</h1>

        <div class="col-sm-8">
            {{  Auth::user() }}
            @yield('content')

        </div>

        <div class="col-sm-4">
            @yield('sidebar')
        </div>

    </div>

    {{ HTML::script('scripts/vendor/jquery.js') }}
    {{ HTML::script('scripts/vendor/bootstrap.js') }}

    @yield('orfooter', '<!-- Nothing Defined -->')

    </body>

</html>
