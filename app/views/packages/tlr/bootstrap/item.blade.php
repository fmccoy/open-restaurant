@if( $item->hasItems() )

<li class="dropdown">

	@if( $item->option('link') )
		{{ HTML::element( $item->option('element', 'a'), array('href' => $item->option('link'), 'data-toggle'=>'dropdown'), $item->option('title').' <b class="caret"></b>' ) }}
	@else
		{{ HTML::element( $item->option('element', 'a'), array('data-toggle'=>'dropdown'), $item->option('title').' <b class="caret"></b>' ) }}
	@endif

	<ul class="dropdown-menu">
		@foreach( $item->getItems() as $subItem )
			{{ View::make('menu::item')->with('item', $subItem) }}
		@endforeach
	</ul>

@else

<li>

	@if( $item->option('link') )
		{{ HTML::element( $item->option('element', 'a'), array('href' => $item->option('link')), $item->option('title') ) }}
	@else
		{{ HTML::element( $item->option('element', 'a'), array(), $item->option('title') ) }}
	@endif

@endif

</li>
