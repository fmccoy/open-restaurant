@section('content')
	<h1>Create Employee</h1>

	{{ HTML::ul($errors->all()) }}

	{{ Form::model($employee,array('route'=>array('employees.update',$employee->id),'method'=>'PUT', 'class'=>'form form-horizontal' )) }}

		<div class="form-group">
			{{ Form::label('firstName', 'First Name:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('first_name', null, array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('lastName', 'Last Name:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('last_name', null, array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('email', 'Email Address:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::email('email', null, array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('phoneNumber', 'Phone Number:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('phone_number', null, array('class'=>'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('position', 'Position:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{	Form::select('position', array(
							'boh-cook'		=> 'Cook',
							'boh-dish'		=> 'Dish',
							'boh-manager'	=> 'Chef',
							'foh-server'	=> 'Server',
							'foh-bar'			=> 'Bar',
							'foh-manager' => 'Manager'
						), null, array('class' => 'form-control'))
				}}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('availability', 'Availability:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				<div class="table table-responsive">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th> </th>
								<th>Sunday</th>
								<th>Monday</th>
								<th>Tuesday</th>
								<th>Wednesday</th>
								<th>Thursday</th>
								<th>Friday</th>
								<th>Saturday</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="shift">AM</td>
								<td>{{ Form::select("availability[sunday][am]",array(0 =>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[monday][am]",array(0 =>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[tuesday][am]",array(0 =>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[wednesday][am]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[thursday][am]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[friday][am]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[saturday][am]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
							</tr>
							<tr>
								<td class="shift">PM</td>
								<td>{{ Form::select("availability[sunday][pm]",array(0 =>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[monday][pm]",array(0 =>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[tuesday][pm]",array(0 =>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[wednesday][pm]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[thursday][pm]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[friday][pm]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
								<td>{{ Form::select("availability[saturday][pm]",array(0=>'NO',1=>'YES'),null,array('class'=>'form-control')) }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>			
		</div>
		

		{{ Form::submit('Submit', array('class' => 'btn btn-default')) }}

	{{ Form::close() }}
@stop