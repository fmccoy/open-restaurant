@section('content')

<h1>Employees</h1>

<!-- will be used to show any messages -->
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Position</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($employees as $employee)
    	<tr>
    		<td>{{ $employee->id }}</td>
    		<td>{{ $employee->first_name }} {{ $employee->last_name }}</td>
    		<td>{{ $employee->position }}</td>
    		<td>
    			{{ Form::open(array('url' => 'emp/' . $employee->id, 'class' => 'pull-right')) }}
    				{{ Form::hidden('_method', 'DELETE') }}
    				<button type="submit" class="btn btn-warning">
                      <span class="glyphicon glyphicon-trash"></span>
                    </button>
    			{{ Form::close() }}

    			<a class="btn btn-small btn-success" href="{{ URL::to('employees/' . $employee->id) }}">
                    <span class="glyphicon glyphicon-info-sign"></span>
                </a>

    			<a class="btn btn-small btn-info" href="{{ URL::to('employees/' . $employee->id . '/edit') }}">
                    <span class="glyphicon glyphicon-edit"></span>
                </a>
    		</td>
    	</tr>
    @endforeach
	</tbody>
</table>

@stop