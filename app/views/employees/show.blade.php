@section('content')
	<h1>Employee: {{ $employee->first_name ." ".  $employee->last_name }}</h1>

	{{ HTML::ul($errors->all()) }}

	{{ Form::model($employee,array('route'=>array('employees.update',$employee->id),'method'=>'PUT', 'class'=>'form form-horizontal')) }}

		<div class="form-group">
			{{ Form::label('firstName', 'First Name:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('first_name', null, array('class'=>'form-control', 'disabled')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('lastName', 'Last Name:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('last_name', null, array('class'=>'form-control', 'disabled')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('email', 'Email Address:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::email('email', null, array('class'=>'form-control', 'disabled')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('phoneNumber', 'Phone Number:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{ Form::text('phone_number', null, array('class'=>'form-control', 'disabled')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('position', 'Position:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				{{	Form::select('position', array(
							'boh-cook'		=> 'Cook',
							'boh-dish'		=> 'Dish',
							'boh-manager'	=> 'Chef',
							'foh-server'	=> 'Server',
							'foh-bar'			=> 'Bar',
							'foh-manager' => 'Manager'
						), null, array('class' => 'form-control', 'disabled'))
				}}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('availability', 'Availability:', array('class' => 'col-sm-2 control-label')) }}
			<div class="col-sm-10">
				<div class="table table-responsive">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th> </th>
								<th>Sunday</th>
								<th>Monday</th>
								<th>Tuesday</th>
								<th>Wednesday</th>
								<th>Thursday</th>
								<th>Friday</th>
								<th>Saturday</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="shift">AM</td>
								@if($employee->availability->sunday->am)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->monday->am)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->tuesday->am)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif
								
								@if($employee->availability->wednesday->am)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->thursday->am)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->friday->am)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->saturday->am)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif
							</tr>
							<tr>
								<td class="shift">PM</td>
								@if($employee->availability->sunday->pm)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->monday->pm)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->tuesday->pm)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif
								
								@if($employee->availability->wednesday->pm)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->thursday->pm)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->friday->pm)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif

								@if($employee->availability->saturday->pm)
									<td class="success"></td>
								@else
									<td class="danger"></td>
								@endif
							</tr>
						</tbody>
					</table>
				</div>		
				
			</div>
				
		</div>

	{{ Form::close() }}
@stop