<?php

class MenuComposer {

	public function compose($view)
	{
		$view->with('menu', Site::getOptions());
	}
}