<?php

class SiteComposer {

	public function compose($view)
	{
		$view->with('site', Site::getOptions());
	}
}