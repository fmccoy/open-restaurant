<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function($table)
		{
			$table->increments('id');
			$table->integer('user_id')->foreign()->references('id')->on('users');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('phone_number');
			$table->string('emergency_contact');
			$table->text('availability');
			$table->string('position');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}

}
