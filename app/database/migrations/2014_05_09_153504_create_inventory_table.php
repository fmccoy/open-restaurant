<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventory', function($table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('purchasing_unit_qty');
			$table->string('purchasing_unit');
			$table->float('purchasing_unit_price');
			$table->string('recipe_unit');
			$table->integer('recipe_conversion');
			$table->string('ind_count_unit');
			$table->integer('inventory_conversion');
			$table->integer('on_hand');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventory');
	}

}
