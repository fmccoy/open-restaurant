<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
        $user = new User;
        $user->username = 'admin';
        $user->email = 'frankmccoy.d@gmail.com';
        $user->password = 'password';
        $user->password_confirmation = 'password';
        $user->confirmation_code = md5(unique(mt_rand(), true));

        if (! $user->save()) {
            Log::info('Unable to create user ' . $user->username, (array)$user->errors());
        } else {
            Log::info('Created user ' . $user->username . '"<' . $user->email . '>' );
        }
    }
}
