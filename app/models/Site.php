<?php

class Site extends Eloquent {

	public $site_name;

	public static $options = array(
		'site_name' => 'Default Name',
		'site_url'	=> 'http://localhost/:8001',
		'admin_email'=> 'frankmccoy.d@gmail.com',
		'week_start' => 'wednesday', // (e.g. sunday, monday,tuesday, wednesday, thursday, friday, saturday)
	);

	public static function getOptions()
	{
		return self::$options;
	}

	public static function getOption($name)
	{
		$options = self::getOptions();
		if(array_key_exists($name, $options)){
			$option = $options[$name];
		} else {
			$option = null;
		}
		return $option;
	}

	public static function setOptions($options)
	{
		$options = array_unique( array_merge( self::$options, $options ) );
		self::$options = $options;
	}

	public static function menu($admin = false)
	{
		$menu = Menu::menu('main-nav', array('class'=>'nav navbar'));
		$menu->setView( 'packages.tlr.bootstrap.main' );
		
		if( $admin == true ) {
			$schedules = $menu->item('schedules','Schedules','#');
				$viewSched = $schedules->item('schedules-view', 'View Schedules', URL::to('schedules'));
				$creteSched = $schedules->item('schedules-create', 'Create Schedule', URL::to('schedules/create'));
			$employees = $menu->item('employees', 'Employees', '#');
				$viewEmployees = $employees->item('employees-view', 'View Employees', URL::to('employees'));
				$createEmployees = $employees->item('employees-create', 'Create Employee', URL::to('employees/create'));
			$inventory = $menu->item('inventory', 'Inventory', '#');
				$viewInventory = $inventory->item('inventory-view', 'View Inventory', URL::to('inventory'));
				$createInventory = $inventory->item('inventory-create', 'Add Item', URL::to('inventory/create'));
			$recipes = $menu->item('recipes', 'Recipes', '#');
				$viewRecipes = $recipes->item('recipes-view', 'View Recipes', URL::to('recipes'));
				$createRecipes = $recipes->item('recipes-create', 'Create Recipe', URL::to('recipes/create'));
		} else {
			//
		}

		// Check if logged in
		if( Auth::check() ) {
			$logout = $menu->item('logout', 'Logout', '#logout');
		} else {
			$login = $menu->item('login', 'Login', '#login');
		}
	}


}