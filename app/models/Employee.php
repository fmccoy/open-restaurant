<?php

class Employee extends Eloquent {

	protected $table = 'employees';

	public function user()
	{
		return $this->belongsTo('User', 'user_id', 'id');
	}

	public function getAvailabilityAttribute($value)
	{
		return json_decode($value);
	}

	public static function whatDepartment($id)
	{
		$employee = Employee::findOrFail($id);
		
		$department = null;

		if(strpos($employee->position, 'boh-') !== false){
			$department = 'boh';
		}
		
		if(strpos($employee->position, 'foh-') !== false){
			$department = 'foh';
		}
		
		return $department;
	}
}