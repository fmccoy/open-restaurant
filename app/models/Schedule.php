<?php

class Schedule extends Eloquent {

	protected $table = 'schedules';

	//protected $week_start = Site::getOption('week_start');

	/**
	 * Return the days of the week ordered by defined week start
	 * 
	 * return array()
	 */
	public static function getWeek()
	{
		$timestamp = strtotime(Site::getOption('week_start'));
		$days = array();
		
		for ($i = 0; $i < 7; $i++) {
		 $days[] = strtolower(strftime('%A', $timestamp));
		 $timestamp = strtotime('+1 day', $timestamp);
		}
		return $days;
	}

	public static function whosAvailable($department, $shift = array())
	{
		$employees = Employee::all();
		$available = array();
		foreach($employees as $employee){
			$position = Employee::whatDepartment($employee['id']); 
			if($position == $department){
				
				if(!empty($shift)){
					$canworkwhen = json_decode(json_encode($employee['availability'], true));
					//$string = printf('$canworkwhen['.$shift["day"].']['.$shift["day"].']', $shift['day'], $shift['time']);
					echo "<pre>";
					print_r($canworkwhen->$shift['day']->$shift['time']);
					echo "</pre>";
					if($canworkwhen->$shift['day']->$shift['time'] == 1 ){
						$available[] = $employee;
					}
				}
			}
		}

	 return $available;
	}
	
}